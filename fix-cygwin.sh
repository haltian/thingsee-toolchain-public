#!/usr/bin/env bash

set -e

[ "$OSTYPE" != "cygwin" ] && \
	echo "This is not a Cygwin terminal. Do not run this script" 1>&2 && \
	exit 1

! cmd /c net session >/dev/null 2>&1 && \
	echo "The Cygwin terminal needs to be launched as Administrator to function correctly" 1>&2 && \
	exit 1

echo This script will make your Cygwin environment compatible with Thingsee 1>&2
echo toolchain. You need to run it only once 1>&2

echo "....Re-mapping Windows home folder" 1>&2
mkdir -p /Users
mount -f $(cygpath -Hw) /Users
cp /etc/fstab /etc/fstab~
mount -m >/etc/fstab

# If any path component, which leads to user's home folder has Unicode
# characters, then the problem is imminent. Boot2docker Linux is
# misconfigured and puts LANG=C. This makes Unicode filenames show
# corrupted in the shared folder (on OS X they are not shown). As a
# workaround we will create a separate folder under /Users with a safe
# name. Then we will create a soft link to it under $USERPROFILE
if [ "$USERPROFILE" != "$(echo $USERPROFILE | iconv -t ISO8859-1 2>/dev/null)" ]
then
	echo "    The path to your Windows user home folder contains Unicode characters" 1>&2
	echo "    At this time due to a misconfiguration in the Boot2Docker Linux such" 1>&2
	echo "    filenames can not be handled correctly. As a workaround the script will" 1>&2
	echo "    create a 'thingsee' symbolic link in $USERPROFILE" 1>&2
	echo "    and it will point to a safe location. You should place Thingsee SDK there" 1>&2
	rm -f $(cygpath $USERPROFILE/thingsee)
	SAFE_HOME_FOLDER=/Users/ThingseeSafe-$(id -u)
	# May exist already
	mkdir -p $SAFE_HOME_FOLDER
	cmd /c chcp 65001 >/dev/null 2>&1
	cmd /c mklink /d %userprofile%\\thingsee $(cygpath -w $SAFE_HOME_FOLDER)
else
	SAFE_HOME_FOLDER=/Users/$(basename $USERPROFILE)
fi

echo "....Changing home folder of the current user" 1>&2
cp /etc/passwd /etc/passwd~
awk -F: "BEGIN {FS=OFS=\":\"}
{
	if (\$1 == \"$(id -un)\")
	{
		\$6=\"$SAFE_HOME_FOLDER\"
	}
	print \$0;
}" </etc/passwd~ >/etc/passwd

echo Configuration succeeded. Re-start your terminal now in Administrator mode 1>&2
echo and begin to use Thingsee toolchain 1>&2
