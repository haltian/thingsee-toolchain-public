# Best effort to support different shells

: ${THINGSEE_TOOLCHAIN_VM:=boot2docker-vm}

is_cygwin() {
    [ "$OSTYPE" = "cygwin" ]
}

is_osx() {
    [ "$(uname)" = "Darwin" ]
}

is_linux() {
    [ "$(uname)" = "Linux" ]
}

# Cheap and dirty way
if is_cygwin
then
    # Eclipse forces Cygwin to return Windows-style paths
    abs_path() {
        (cd $1 && cygpath $(pwd))
    }
    translate_path() {
        cygpath -w "$@"
    }
else
    abs_path() {
        (cd $1 && pwd)
    }
    translate_path() {
        echo "$@"
    }
fi

is_in_array() {
    local element
    for element in "${@:2}"
    do
        [ "$element" = "$1" ] && return 0
    done
    return 1
}

__thingsee_wants_help()
{
    is_in_array "--help" "$@" || is_in_array "-h" "$@"
}

# Cross-platform temporary file/folder creation
__thingsee_mktemp() {
    local tmp_dir=${TMPDIR:-/tmp}
    mktemp "$@" $tmp_dir/XXXXX
}

__thingsee_grep() {
    # Different breeds of grep
    if is_osx
    then
        grep --text -E "$@"
    else
        grep --text -P "$@"
    fi
}

__thingsee_md5() {
    if is_osx
    then
        cat "$@" | perl -e 'use Digest::MD5; $ctx=Digest::MD5->new; $ctx->addfile(STDIN); print $ctx->hexdigest'
    else
        md5sum "$@" | awk '{print $1}'
    fi
}

if is_cygwin || is_osx
then
    [ -o xtrace ] && boot2docker_be_verbose='-v'
    boot2docker="boot2docker --vm=$THINGSEE_TOOLCHAIN_VM"
fi

# TODO: docker binaries do not exist in Windows yet..
if is_cygwin
then
    DOCKER="$boot2docker ssh docker"
    DOCKER_TTY="$boot2docker ssh -t docker"
else
    DOCKER=docker
    DOCKER_TTY=docker
fi

if is_linux
then
    __thingsee_prepare_docker() {
        : Nothing to do, we are ready
    }
else
    if [ -z "$DONT_ASSIGN_THINGSEE_SH_CURRENT_MD5" ]
    then
        THINGSEE_SH_CURRENT_MD5=$(__thingsee_md5 ${BASH_SOURCE[0]})
        THINGSEE_SH_MD5=$THINGSEE_SH_CURRENT_MD5
    else
        # Always one-shot
        unset DONT_ASSIGN_THINGSEE_SH_CURRENT_MD5
    fi
    if is_osx
    then
        __thingsee_vbm() {
            VBoxManage "$@"
        }
    else
        __thingsee_vbm() {
            local vbm=$(cygpath "$VBOX_MSI_INSTALL_PATH")VBoxManage.exe
            # Quoted because of Windows 'Program Files' path
            "$vbm" "$@"
        }
    fi
    __thingsee_get_extpack() {
        __thingsee_vbm list extpacks | __thingsee_grep -q 'Pack no. \d+:\s+Oracle VM VirtualBox Extension Pack' && \
            return 0
        local vb_version=$(__thingsee_vbm --version | awk -Fr '{print $1}')
        ! echo $vb_version | __thingsee_grep -q '^\d+\.\d+\.\d+$' && \
            echo "Failed to retrieve VirtualBox version ($vb_version)" 1>&2 && \
            return 1
        local extpack_path=$(__thingsee_mktemp -d)
        local extpack_file=Oracle_VM_VirtualBox_Extension_Pack-${vb_version}.vbox-extpack
        curl -Lo $extpack_path/$extpack_file "http://download.virtualbox.org/virtualbox/${vb_version}/$extpack_file" || \
            return 1 # Should print own error message
        echo Oracle VirtualBox Extension pack needs to be installed for Thingsee toolchain to function.
        echo Your OS may or may not ask you to confirm administrative action
        __thingsee_vbm extpack install $(translate_path $extpack_path/$extpack_file) || \
            return 1 # Should print own error message
    }
    __thingsee_setup_usb() {
        __thingsee_vbm modifyvm $THINGSEE_TOOLCHAIN_VM --usb on --usbehci on || \
            return 1 # Should print own error message
        while __thingsee_vbm usbfilter remove 0 --target $THINGSEE_TOOLCHAIN_VM >/dev/null 2>&1
        do
            : Remove all filters. No custom filters are expected
        done
        __thingsee_setup_usb_filter() {
            __thingsee_vbm usbfilter add 0 \
                --target $THINGSEE_TOOLCHAIN_VM \
                --name "$1" \
                --vendorid "$2" --productid "$3"
        }
        __thingsee_setup_usb_filter "Thingsee DFU mode" "0483" "df11" || \
            return 1
        __thingsee_setup_usb_filter "Thingsee ACM mode" "0525" "a4a7" || \
            return 1
        __thingsee_setup_usb_filter "Olimex OpenOCD JTAG" "15ba" "0003" || \
            return 1
        __thingsee_setup_usb_filter "Olimex OpenOCD-H JTAG" "15ba" "002b" || \
            return 1
    }
    __thingsee_get_vm_info() {
        __thingsee_vbm showvminfo $THINGSEE_TOOLCHAIN_VM --machinereadable
    }
    __thingsee_setup_port_forwarding() {
        if __thingsee_get_vm_info | __thingsee_grep -q "^Forwarding\(\d+\)=\"$1,"
        then
            __thingsee_vbm modifyvm $THINGSEE_TOOLCHAIN_VM --natpf1 delete $1 || \
                return 1
        fi
        __thingsee_vbm modifyvm $THINGSEE_TOOLCHAIN_VM \
            --natpf1 "$1,tcp,,$2,,$3"
    }
    __thingsee_setup_ports() {
        __thingsee_setup_port_forwarding "GDB" $THINGSEE_TOOLCHAIN_GDBPORT $THINGSEE_TOOLCHAIN_GDBPORT
        __thingsee_setup_port_forwarding "OCD" $THINGSEE_TOOLCHAIN_OCDPORT $THINGSEE_TOOLCHAIN_OCDPORT
    }
    __thingsee_setup_shares_and_start() {
        # Start does not always return error, where it should
        # but shellinit will, if the VM is in broken state
        $boot2docker $boot2docker_be_verbose --vbox-share=$(translate_path /Users)=Users start && \
            $boot2docker shellinit >/dev/null 2>&1
    }
    __thingsee_get_vm_parameter() {
        __thingsee_get_vm_info | __thingsee_grep "^$1=\"" | tr -d '"' | awk -F= '{print $2}'
    }
    __thingsee_disable_uart() {
        # There is a bug in boot2docker-cli (https://github.com/boot2docker/boot2docker-cli/issues/337)
        # It is impossible to start multiple VMs in Windows, because UARTs are assigned the same
        # pipe name. We do not use pipes, so we will disable the serial port
        __thingsee_vbm modifyvm $THINGSEE_TOOLCHAIN_VM \
            --uart1 off
    }
    __thingsee_natdnshostresolver1_on() {
        # Windows workaround: when the local network connection is flaky, then updates in DNS status
        # affect to NAT network - even simple ssh connection can get refused
        # For now this is activated only on Windows. Did not see it on OS X
        ! is_cygwin || \
        __thingsee_vbm modifyvm $THINGSEE_TOOLCHAIN_VM \
            --natdnshostresolver1 on
    }
    __thingsee_boot2docker_status() {
        # In verbose mode there can be extra-lines on STDOUT
        $boot2docker status
    }
    __thingsee_print_aborted_vm_note() {
        if [ -n "$1" ]
        then
            echo "Initialization failed, because the virtual machine was not shut down correctly" 1>&2
            echo "The contents are likely to have become corrupted. Run to start over:" 1>&2
            echo "    $boot2docker destroy" 1>&2
            echo "    thingsee envsetup" 1>&2
            echo "This only affects to the toolchain itself and no other data shall be lost" 1>&2
        fi
        return 1
    }
    __thingsee_prepare_docker() {
        local provisioned_line_prefix="Provisioned for thingsee toolchain: "
        if is_cygwin
        then
            # In case of Windows we have to make sure the default codepage
            # for applications running through CMD is unicode
            cmd /c chcp 65001 >/dev/null 2>&1
        fi
        __thingsee_get_extpack || \
            return 1
        # Repeated initialization of the VM is not an error
        $boot2docker $boot2docker_be_verbose \
            --memory=1024 \
            --disksize=$THINGSEE_TOOLCHAIN_VM_DISK_SIZE \
            --sshport=$THINGSEE_TOOLCHAIN_SSHPORT \
            init >/dev/null || true
        # But the command should not fail, if the initialization succeeded
        $boot2docker status >/dev/null || \
            return 1
        if [ "$($boot2docker status)" = "aborted" ]
        then
            local vm_aborted=1
        fi
        if [ "$($boot2docker status)" = "paused" ] || [ "$($boot2docker status)" = "saved" ]
        then
                $boot2docker up
        fi
        if [ "$($boot2docker status)" != "poweroff" ] && [ "$($boot2docker status)" != "aborted" ]
        then
            if [ -z "$THINGSEE_SKIP_SYNC_CHECK" ] && [ "$(__thingsee_get_vm_parameter description)" != "${provisioned_line_prefix}${THINGSEE_SH_MD5}" ]
            then
                echo "The $THINGSEE_TOOLCHAIN_VM needs to be turned off and configured." 1>&2
                echo "Run the following command and then do 'thingsee envsetup' again:" 1>&2
                echo "    boot2docker --vm=$THINGSEE_TOOLCHAIN_VM stop" 1>&2
                echo "NB! All running containers will be shut down!" 1>&2
                return 1
            fi
        fi
        until [ "$($boot2docker status)" = "running" ]
        do
            local vm_status=$($boot2docker status)
            local accumulator=""
            case $vm_status in
                "poweroff"|"aborted")
                    __thingsee_disable_uart || \
                        __thingsee_print_aborted_vm_note $vm_aborted || \
                        return 1
                    __thingsee_setup_usb || \
                        __thingsee_print_aborted_vm_note $vm_aborted || \
                        return 1
                    __thingsee_setup_ports || \
                        __thingsee_print_aborted_vm_note $vm_aborted || \
                        return 1
                    __thingsee_natdnshostresolver1_on || \
                        __thingsee_print_aborted_vm_note $vm_aborted || \
                        return 1
                    __thingsee_vbm modifyvm $THINGSEE_TOOLCHAIN_VM \
                        --description "${provisioned_line_prefix}${THINGSEE_SH_MD5}" || \
                        return 1
                    __thingsee_setup_shares_and_start || \
                        __thingsee_print_aborted_vm_note $vm_aborted || \
                        return 1 # Should print own error message
                    ;;
                "")
                    echo "Error retrieving boot2docker status" 1>&2 && \
                        return 1
                    ;;
                *)
                    sleep 1 # This is intermediate state
                    local accumulator="${accumulator}0"
                    if [ "${#accumulator}" == "10" ]
                    then
                        echo "BUG! The VM did not reach power off state before timeout. Current state is $vm_state" 1>&2
                        return 1
                    fi
                    ;;
            esac
        done
        if is_osx
        then
            # The VM is up and running now
            local shellinit=$(__thingsee_mktemp)
            ! $boot2docker shellinit >$shellinit 2>/dev/null &&
                echo "Failed to setup docker connection" 1>&2 &&
                return 1
            source $shellinit
        fi
    }
fi

if [ -z "$DONT_ASSIGN_BASE_ENVSETUP_FOLDER" ]
then
    __thingsee_sh_path() {
        if [ -n "$BASH_VERSION" ]
        then
            # This is an array, but to get this working in ash
            # we have to access it like this - otherwise the ash
            # complains about syntax errors
            echo ${BASH_SOURCE[0]}
        else
            echo $_
        fi
    }

    __thingsee_sh_downloaded() {
        local arg=$1
        # /dev/stdin case
        [ "$arg" = "/dev" ] && return 0
        # /dev/fd/xx case
        arg=$(dirname $arg)
        [ "$arg" = "/dev" ] && return 0
        # /proc/xx/fd case
        arg=$(dirname $arg)
        [ "$arg" = "/proc" ] && return 0
        return 1
    }
    BASE_ENVSETUP_FOLDER=$(abs_path $(dirname $(__thingsee_sh_path)))
    if __thingsee_sh_downloaded $BASE_ENVSETUP_FOLDER
    then
        # sourced from STDIN
        BASE_ENVSETUP_FOLDER=$(abs_path .)
    fi
    if [ -r $BASE_ENVSETUP_FOLDER/config.sh ]
    then
        source $BASE_ENVSETUP_FOLDER/config.sh
    fi

    # These are configurable
    : ${THINGSEE_TOOLCHAIN_IMAGE:=haltian/thingsee-toolchain}
    # TODO: Perhaps we should use different VM name by default
    # TODO: The VM has to be off before first thingsee envsetup
    # TODO: so all settings can be configured.
    : ${THINGSEE_TOOLCHAIN_VM_DISK_SIZE:=20000}
    : ${THINGSEE_TOOLCHAIN_SSHPORT:=2022}
    : ${THINGSEE_TOOLCHAIN_GDBPORT:=3333}
    : ${THINGSEE_TOOLCHAIN_OCDPORT:=4444}
else
    # This is always only one-shot
    unset DONT_ASSIGN_BASE_ENVSETUP_FOLDER
fi

# Helps to verify that all containers are cleaned up
# properly
__thingsee_container_name() {
    # The first parameter is a fallback value to make sure
    # it is assigned even without global control
    : ${THINGSEE_TOOLCHAIN_FORCE_CONTAINER_NAME:=$1}
    if [ -n "$THINGSEE_TOOLCHAIN_FORCE_CONTAINER_NAME" ]
    then
        echo "--name=$THINGSEE_TOOLCHAIN_FORCE_CONTAINER_NAME"
    else
        echo ""
    fi
}

__thingsee_diag_print() {
    # TODO: stderr or stdout?
    echo "$@" 1>&2
}

__thingsee_envsetup_help() {
    if is_linux || is_osx
    then
        echo "Usage: thingsee envsetup [--help|-h] [--native]" 1>&2
        echo "  Enhance current shell with updated toolchain commands" 1>&2
        echo "  --help      - print this message" 1>&2
        echo "  --native    - setup for native builds" 1>&2
    else
        echo "Usage: thingsee envsetup [--help]" 1>&2
        echo "  Enhance current shell with updated toolchain commands" 1>&2
        echo "  --help      - print this message" 1>&2
    fi
}

__thingsee_envsetup() {
    __thingsee_wants_help "$@" && \
        __thingsee_envsetup_help && \
        return
    if is_in_array "--native" "$@"
    then
        THINGSEE_NATIVE_BUILDS=1
    else
        unset THINGSEE_NATIVE_BUILDS
    fi
    if [ -n "$THINGSEE_NATIVE_BUILDS" ]
    then
        __thingsee_diag_print "...Checking pre-conditions for native builds"
        ! which make &>/dev/null && \
            __thingsee_diag_print "   !!Can not find 'make'. Native builds are impossible..." && \
            return 1
        ! which arm-none-eabi-gcc &>/dev/null && \
            __thingsee_diag_print "   !!Can not find 'arm-none-eabi-gcc'. Native builds are impossible..." && \
            return 1
        [ "$(arm-none-eabi-gcc -dumpversion | awk -F. '{print $1 * 10000 + $2 * 100 + $3}')" -lt "40900" ] && \
            __thingsee_diag_print "   !!arm-none-eabi-gcc version is $(arm-none-eabi-gcc -dumpversion), while 4.9+ is required. Native builds are impossible..." && \
            return 1
        ! which arm-none-eabi-g++ &>/dev/null && \
            __thingsee_diag_print "   !!Can not find 'arm-none-eabi-g++'. Native builds are impossible..." && \
            return 1
        ! which arm-none-eabi-ld &>/dev/null && \
            __thingsee_diag_print "   !!Can not find 'arm-none-eabi-ld'. Native builds are impossible..." && \
            return 1
        ! which arm-none-eabi-ar &>/dev/null && \
            __thingsee_diag_print "   !!Can not find 'arm-none-eabi-ar'. Native builds are impossible..." && \
            return 1
        ! which arm-none-eabi-nm &>/dev/null && \
            __thingsee_diag_print "   !!Can not find 'arm-none-eabi-nm'. Native builds are impossible..." && \
            return 1
        ! which arm-none-eabi-objcopy &>/dev/null && \
            __thingsee_diag_print "   !!Can not find 'arm-none-eabi-objcopy'. Native builds are impossible..." && \
            return 1
        ! which arm-none-eabi-objdump &>/dev/null && \
            __thingsee_diag_print "   !!Can not find 'arm-none-eabi-objdump'. Native builds are impossible..." && \
            return 1
        ! which gcc &>/dev/null && \
            __thingsee_diag_print "   !!Can not find 'gcc'. Native builds are impossible..." && \
            return 1
        ! which g++ &>/dev/null && \
            __thingsee_diag_print "   !!Can not find 'g++'. Native builds are impossible..." && \
            return 1
        ! which ld &>/dev/null && \
            __thingsee_diag_print "   !!Can not find 'ld'. Native builds are impossible..." && \
            return 1
        ! which ar &>/dev/null && \
            __thingsee_diag_print "   !!Can not find 'ar'. Native builds are impossible..." && \
            return 1
        ! which nm &>/dev/null && \
            __thingsee_diag_print "   !!Can not find 'nm'. Native builds are impossible..." && \
            return 1
        ! which objcopy &>/dev/null && \
            __thingsee_diag_print "   !!Can not find 'objcopy'. Native builds are impossible..." && \
            return 1
        ! which objdump &>/dev/null && \
            __thingsee_diag_print "   !!Can not find 'objdump'. Native builds are impossible..." && \
            return 1
        ! which git &>/dev/null && \
            __thingsee_diag_print "   !!Can not find 'git'. Native builds are impossible..." && \
            return 1
        ! which kconfig-mconf &>/dev/null && \
            __thingsee_diag_print "   !!Can not find 'kconfig-mconf'. Configuration GUI will be dockerized..."
    fi
    __thingsee_diag_print "...Preparing the Docker environment"
    __thingsee_prepare_docker || \
        return 1
    local tmp=$(__thingsee_mktemp)
    # The image identification may include a tag
    local pullable_image=${THINGSEE_TOOLCHAIN_IMAGE/:*/}
    if [ "$($DOCKER images -q $pullable_image)" = "" ]
    then
        __thingsee_diag_print "...Pulling $pullable_image from docker.io. Please be patient"
        # Download in interactive mode, so the prgress is shown
        $DOCKER_TTY pull $pullable_image
    fi
    __thingsee_diag_print "...Activating all Thingsee toolchain commands"
    $DOCKER run --rm $(__thingsee_container_name) $THINGSEE_TOOLCHAIN_IMAGE envsetup >$tmp
    # BASE_ENVSETUP_FOLDER is not touched there
    source $tmp
    rm -f $tmp
    # It is possible to ignore outdate check
    [ -n "$THINGSEE_SKIP_SYNC_CHECK" ] && return 0
    if [ "$THINGSEE_SH_CURRENT_MD5" != "$THINGSEE_SH_MD5" ]
    then
        # sourced thingsee.sh is different from what we have inside the image
        echo "Your sourced thingsee.sh file does not match the one inside" 1>&2
        echo "${THINGSEE_TOOLCHAIN_IMAGE}. The file includes important" 1>&2
        echo "configuration for the VM. Current version of the script can be" 1>&2
        echo "downloaded from:" 1>&2
        echo "    http://thingsee.org/toolchain/thingsee.sh" 1>&2
        echo "Then your terminal needs to be provisioned again:" 1>&2
        echo "    source thingsee.sh" 1>&2
        echo "    thingsee envsetup" 1>&2
        echo "It is possible you will have to shutdown the virtual machine and" 1>&2
        echo "you can do it as follows:" 1>&2
        echo "    boot2docker --vm=$THINGSEE_TOOLCHAIN_VM stop" 1>&2
        return 1
    fi
}

__thingsee_sdk()
{
    if __thingsee_wants_help $1
    then
        echo "Usage: thingsee sdk [--help|-h]" 1>&2
        echo "  Prints location of the current Thingsee SDK. Run 'thingsee envsetup' to get more complete context help" 1>&2
    else
        echo $BASE_ENVSETUP_FOLDER
    fi
}

# This function is replaced after the real envsetup is sourced
__thingsee_help()
{
    echo "Usage: thingsee <command>"
    echo ""
    echo "Thingsee toolchain front-end supports the following commands:"
    echo "  help       - This help message"
    echo "  sdk        - Print path to currently selected SDK"
    echo "  envsetup   - Enhance current shell with updated toolchain commands"
}

is_ssh_localhost_ok()
{
    dnsdomainname >/dev/null 2>&1 || cat ~/.ssh/config 2>/dev/null | __thingsee_grep -q '^Host\s+localhost'
}

get_home_folder_from_etc_passwd()
{
        # In Cygwin $USER is not assigned, when we use toolchain inside Makefile
        : ${USER:=$USERNAME}
    grep -P "^$USER:" /etc/passwd | awk -F: '{print $6}'
}

# This function is replaced after the real envsetup is sourced
if is_cygwin && ! net session >/dev/null 2>&1
then
    # Nothing is guaranteed, if we are not Windows admin
    thingsee()
    {
        echo "The Cygwin terminal needs to be launched as Administrator to function correctly" 1>&2
        echo "Thingsee toolchain will not work"
        return 1
    }
    # Formally the sourcing succeeds
    thingsee || true
elif is_cygwin && ! is_ssh_localhost_ok
then
    # If hostname in Windows contains some "international" characters
    # then hostname resolution may fail with
    # http://osxr.org/glibc/ident?_i=EAI_IDN_ENCODE
    # The Windows itself will warn you, if you try to use such characters
    # in your host name. So, as a matter of fact, we refuse to support it
    thingsee()
    {
        echo "Your Windows PC name $(hostname) contains unsupported characters" 1>&2
        echo "The immediate effect of this issue is that 'ssh localhost' will fail to work" 1>&2
        echo "You are advised to modify the Windows PC name. Please follow" 1>&2
        echo "http://windows.microsoft.com/en-us/windows/change-computer-name" 1>&2
        echo "If you can not do that, then the workaround is to place these 2 lines into ~/.ssh/config" 1>&2
        echo "Host localhost" 1>&2
        echo "  HostName 127.0.0.1" 1>&2
        echo "At this time there is no official support for your setting in the toolchain" 1>&2
        return 1
    }
    # Formally the sourcing succeeds
    thingsee || true
elif is_cygwin && ! get_home_folder_from_etc_passwd | grep -qP '^/Users/'
then
    # Apparently, fix-cygwin.sh was not run
    thingsee()
    {
        echo "Your Cygwin terminal is not ready. Please run fix-cygwin.sh and restart Cygwin" 1>&2
        echo "terminal as Administrator" 1>&2
        return 1
    }
    # Formally the sourcing succeeds
    thingsee || true
elif is_cygwin && [ "$(get_home_folder_from_etc_passwd)" != "$HOME" ]
then
    # Forgot to restart the terminal
    thingsee()
    {
        echo "Please restart Cygwin terminal as Administrator for the changes made by" 1>&2
        echo "fix-cygwin.sh take effect" 1>&2
        return 1
    }
    # Formally the sourcing succeeds
    thingsee || true
elif ! is_linux && ! which boot2docker >/dev/null 2>&1
then
    # In OS X and Windows boot2docker has to be installed
    thingsee()
    {
        echo "boot2docker executable is not available. Install Boot2Docker and/or correct your PATH variable." 1>&2
        if is_cygwin
        then
            echo "Please follow https://docs.docker.com/installation/windows" 1>&2
        else
            echo "Please follow https://docs.docker.com/installation/mac" 1>&2
        fi
        return 1
    }
    # Formally the sourcing succeeds
    thingsee || true
elif is_linux && ! groups | grep -qw 'docker'
then
    # In OSX this is not important - docker is accessed through TCP socket
    # In Windows docker is used over SSH - SSH user has everything setup
    # If we can sudo, then we can join docker group
    thingsee()
    {
        # Does not hurt and helps testing inside docker container
        : ${USER:=$(id -un)}
        echo "Thingsee toolchain requires your user (${USER}) to belong to the 'docker' group" 1>&2
        echo "Run the following commands (with root privileges):" 1>&2
        if ! grep -q '^docker:' /etc/group
        then
            echo "    groupadd docker" 1>&2
        fi
        echo "    gpasswd -a ${USER} docker" 1>&2
        echo "You may have to login again" 1>&2
        return 1
    }
    thingsee || true
else
    thingsee()
    {
        local arg=${1:-help}
        shift
        case $arg in
            envsetup)
                __thingsee_envsetup "$@"
                ;;
            sdk)
                __thingsee_sdk "$@"
                ;;
            help | *)
                __thingsee_help
                ;;
        esac
    }
fi

if [ -n "$BASH" ]
then
    # Inclusion of a complete envsetup.sh overrides it
    _thingsee()
    {
        local opts="envsetup sdk help"
        local cur="${COMP_WORDS[COMP_CWORD]}"
        COMPREPLY=( $(compgen -W "$opts" -- $cur) )
        return 0
    }

    complete -F _thingsee thingsee
else
    echo "The Thingsee toolchain is developed and tested with bash interpreter" 1>&2
    echo "This shell is not bash. Thingsee toolchain may or may not work." 1>&2
    echo "Continue using it at your discretion.." 1>&2
fi
